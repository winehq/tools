/* Hide / show reports on the patterns page
 *
 * Copyright 2021 Francois Gouget
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
"use strict";

var hide_reruns;
var groups;
var cbs;
var tests;

function enableExpandButton(test)
{
    test.domexpand.innerHTML = '[+]';
    test.domexpand.title = "Remove filters";
    test.domexpand.style.display = "inline-block";
}

function expandTest(test)
{
    test.patlines.forEach(patline => {
        patline.dom.style.display = patline.display;
        if (!patline.cb.checked)
        {
            const label = patline.dom.querySelector('.label');
            label.style.color = 'darkblue';
        }
    });
    test.domexpand.innerHTML = '[-]';
    test.domexpand.title = "Apply filters";
    test.domexpand.style.display = "inline-block";
    test.expanded = true;
}

function reduceTest(test)
{
    test.patlines.forEach(patline => {
        if (!patline.cb.checked)
            patline.dom.style.display = 'none';
    });
    enableExpandButton(test);
}

function toggleTest(e)
{
    const domtest = e.target.closest("div.testfile");
    const test = tests[domtest.id];
    if (test)
    {
        if (e.target.innerHTML == '[+]')
            expandTest(test);
        else
            reduceTest(test);
    }
}

function refreshPage(changes, rerun_checked)
{
    if (rerun_checked != undefined && rerun_checked != hide_reruns)
    {
        for (let key in cbs)
        {
            const cb = cbs[key];
            if (!cb.rerun) continue;

            cb.dom.disabled = rerun_checked;
            if (rerun_checked)
            {
                cb.dom.parentNode.classList.add("disabled");
                cb.dom.parentNode.title = "Reruns are hidden";
                changes[key] = false;
            }
            else
            {
                cb.dom.parentNode.classList.remove("disabled");
                cb.dom.parentNode.title = undefined;
                changes[key] = groups[cb.group].checked;
            }
            cb.dom.checked = changes[key];
        }
        hide_reruns = rerun_checked;
    }

    for (let key in tests)
    {
        if (tests[key].expanded)
            reduceTest(tests[key]);
    }

    for (let key in changes)
    {
        const cb = cbs[key];
        if (cb.checked == changes[key]) continue;
        if (cb.dom.parentNode.disabled) continue;

        if (changes[key])
        {
            cb.patlines.forEach(patline => {
                patline.dom.style.display = patline.display;
                const label = patline.dom.querySelector('.label');
                label.style.removeProperty('color');
                const test = patline.test;
                test.linecount++;
                if (test.linecount == 1 && test.id != "summary")
                    test.dom.style.display = test.display;
                if (test.linecount == test.patlines.length)
                    test.domexpand.style.display = "none";
            });
        }
        else
        {
            cb.patlines.forEach(patline => {
                patline.dom.style.display = "none";
                const test = patline.test;
                test.linecount--;
                if (test.linecount == 0 && test.id != "summary")
                    test.dom.style.display = "none";
                if (test.linecount == test.patlines.length - 1)
                    enableExpandButton(test);
            });
        }

        cb.checked = changes[key];
    }
}

function toggledRerunCB(e)
{
    refreshPage({}, e.target.checked);
}

function toggledGroupCB(e)
{
    const group = e.target.id;
    const checked = e.target.checked;
    groups[group].checked = checked;

    const changes = {};
    for (let key in cbs)
    {
        const cb = cbs[key];
        if (cb.group == group && !cb.dom.disabled)
        {
            cb.dom.checked = checked;
            changes[key] = checked;
        }
    }
    refreshPage(changes);
}

function toggledReportCB(e)
{
    const cb = e.target;
    const changes = {};
    changes[cb.id] = cb.checked;
    refreshPage(changes);
}

function showRangeCounts(e)
{
    const domtest = e.target.closest("div.testfile");
    domtest.querySelectorAll(":scope [fcount]").forEach(frange => {
        const fcount = frange.getAttribute("fcount");
        frange.innerHTML = fcount.repeat(frange.innerHTML.length);
    });
}

function hideRangeCounts(e)
{
    const domtest = e.target.closest("div.testfile");
    domtest.querySelectorAll(":scope [fcount]").forEach(frange => {
        const fcount = frange.getAttribute("fcount");
        frange.innerHTML = "F".repeat(frange.innerHTML.length);
    });
}

function init()
{
    hide_reruns = false; /* reruns are shown by default */
    const domreruns = document.getElementById("reruns");
    domreruns.addEventListener('click', toggledRerunCB);

    groups = {};
    document.querySelectorAll("input.groupcb").forEach(domcb => {
        groups[domcb.id] = { dom: domcb,
                             checked: domcb.checked
                           };
        domcb.addEventListener('click', toggledGroupCB);
    });

    const changes = {};
    cbs = {};
    document.querySelectorAll("input.reportcb").forEach(domcb => {
        cbs[domcb.id] = { dom: domcb,
                          checked: true, /* all reports are shown by default */
                          display: domcb.style.display,
                          group: domcb.getAttribute("data-group"),
                          rerun: domcb.getAttribute("data-rerun"),
                          patlines: []
                        };
        changes[domcb.id] = domcb.checked;
        domcb.addEventListener('click', toggledReportCB);
    });

    tests = {};
    document.querySelectorAll("div.testfile").forEach(domtest => {
        const domexpand = domtest.querySelector("a.expandtest");
        domexpand.addEventListener('click', toggleTest);

        const test = { id: domtest.id,
                       dom: domtest,
                       display: domtest.style.display,
                       domexpand: domexpand,
                       patlines: [],
                       linecount: 0,
                       expanded: false
                     };
        tests[domtest.id] = test;

        domtest.querySelectorAll("div.patline").forEach(domline => {
            const cb = cbs[domline.getAttribute("data-id")];
            if (cb)
            {
                const patline = { dom: domline,
                                  display: domline.style.display,
                                  cb: cb,
                                  test: test
                                };
                cb.patlines.push(patline);
                test.patlines.push(patline);
                if (cb.checked) test.linecount++;
                const pattern = domline.querySelector("div.pattern");
                pattern.addEventListener('mouseenter', showRangeCounts);
                pattern.addEventListener('mouseleave', hideRangeCounts);
            }
        });
        if (test.linecount < test.patlines.length)
            enableExpandButton(test);
    });

    /* When reloading a page the browser may preserve the checkbox state
     * so reapply them to the rest of the page.
     */
    refreshPage(changes, domreruns.checked);
}

window.addEventListener('load', init);
