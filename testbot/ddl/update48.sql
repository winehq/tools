USE winetestbot;

ALTER TABLE Tasks
  ADD NewWarnings INT(6) NULL
      AFTER Ended,
  ADD Warnings INT(6) NULL
      AFTER NewWarnings,
  ADD NewTestFailures INT(6) NULL
      AFTER Warnings,
  ADD FailedTestUnits INT(6) NULL
      AFTER TestFailures;
