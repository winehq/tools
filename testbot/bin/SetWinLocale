#!/usr/bin/perl -Tw
# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
#
# Sets the locale of the specified Windows machine.
#
# Copyright 2018, 2021-2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

sub BEGIN
{
  if ($0 !~ m=^/=)
  {
    # Turn $0 into an absolute path so it can safely be used in @INC
    require Cwd;
    $0 = Cwd::cwd() . "/$0";
  }
  if ($0 =~ m=^(/.*)/[^/]+/[^/]+$=)
  {
    $::RootDir = $1;
    unshift @INC, "$::RootDir/lib";
  }
}

use WineTestBot::Config;
use WineTestBot::Log;
use WineTestBot::TestAgent;
use WineTestBot::Utils;
use WineTestBot::VMs;

my $HKCU_INTERNATIONAL = "HKCU\\Control Panel\\International";
my $HKCU_GEO = "HKCU\\Control Panel\\International\\Geo";


#
# Error handling and logging
#

my $name0 = $0;
$name0 =~ s+^.*/++;


sub Info(@)
{
  print STDERR "$name0:info: ", @_;
}

sub Warning(@)
{
  print STDERR "$name0:warning: ", @_;
}

sub Error(@)
{
  print STDERR "$name0:error: ", @_;
}

my $Debug;
sub Debug(@)
{
  print STDERR @_ if ($Debug);
}

my $TA;
sub Cleanup()
{
  return if (!$TA);
  if ($Debug)
  {
    Debug("Not deleting the $name0* files for debugging\n");
  }
  else
  {
    $TA->Rm("$name0.out", "$name0.ps1", "$name0.xml");
  }
}

my $Start = Time();
sub FatalError(@)
{
  Error(@_);
  Debug(Elapsed($Start), " Aborting\n");
  Cleanup();
  exit(1);
}


#
# Country and keyboard tables
#

# Map the two-letter country identifier to the Windows country code used in
# the registry. Note that this does not match the CTRY_XXX codes!
my %Countries = (
  "AD" =>   8, # Andorra
  "AE" => 224, # United Arab Emirates
  "AF" =>   3, # Afganistan
  "AL" =>   6, # Albania
  "DZ" =>   4, # Algeria
  "AM" =>   7, # Armenia
  "AR" =>  11, # Argentina
  "AT" =>  14, # Austria
  "AU" =>  12, # Australia
  "AZ" =>   5, # Azerbaijan
  "BA" =>  25, # Bosnia and Herzegovina
  "BE" =>  21, # Belgium
  "BG" =>  35, # Bulgaria
  "BN" =>  37, # Brunei Darussalam
  "BR" =>  32, # Brazil
  "BY" =>  29, # Belarus
  "CA" =>  39, # Canada
  "CH" => 223, # Switzerland
  "CN" =>  45, # China
  "CX" => 309, # Christmas Island
  "CZ" =>  75, # Czech Republic
  "DE" =>  94, # Germany
  "DK" =>  61, # Denmark
  "EE" =>  70, # Estonia
  "EG" =>  67, # Egypt
  "ES" => 217, # Spain
  "ET" =>  73, # Ethiopia
  "FI" =>  77, # Finland
  "FO" =>  81, # Faroe Islands
  "FR" =>  84, # France
  "GB" => 242, # Great-Britain
  "GR" =>  98, # Greece
  "HR" => 108, # Croatia
  "HU" => 109, # Hungary
  "ID" => 111, # Indonesia
  "IE" =>  68, # Ireland
  "IL" => 117, # Israel
  "IN" => 113, # India
  "IR" => 116, # Iran
  "IS" => 110, # Iceland
  "IT" => 118, # Italy
  "KI" => 133, # Kiribati
  "JP" => 122, # Japan
  "KE" => 129, # Kenya
  "KG" => 130, # Kyrgyzstan
  "KR" => 134, # Korea
  "KZ" => 137, # Kazakhstan
  "LT" => 141, # Lithuania
  "LV" => 140, # Latvia
  "MA" => 159, # Morocco
  "MK" => 16618, # Macedonia
  "MN" => 154, # Mongolia
  "MV" => 165, # Maldives
  "MY" => 167, # Malaysia
  "NL" => 176, # Netherlands
  "NO" => 177, # Norway
  "NP" => 178, # Nepal
  "PE" => 187, # Peru
  "PF" => 318, # French Polynesia
  "PK" => 190, # Pakistan
  "PL" => 191, # Poland
  "PT" => 193, # Portugal
  "RO" => 200, # Romania
  "RU" => 203, # Russia
  "SA" => 205, # Saudi Arabia
  "SE" => 221, # Sweden
  "SI" => 212, # Slovenia
  "SK" => 143, # Slovakia
  "SN" => 210, # Senegal
  "TH" => 227, # Thailand
  "TO" => 231, # Tonga
  "TR" => 235, # Turkey
  "TW" => 237, # Taiwan
  "UA" => 241, # Ukraine
  "US" => 244, # USA
  "UZ" => 247, # Uzbekistan
  "VN" => 251, # Vietnam
  "ZA" => 209, # South Africa
);

# Map the locale identifier to the Windows keyboard id
my %Keyboards = (
  "af-ZA" => ["0436:00000409"], # Afrikaans - South Africa
  "ar-EG" => ["0c01:00000401"], # Arabic - Egypt
  "ar-MA" => ["1801:00020401", "040c:0000040c"], # Arabic - Morocco
  "be-BY" => ["0423:00000423", "0419:00000419"], # Belarusian - Belarus
  "bg-BG" => ["0402:00030402", "0409:00020409"], # Bulgarian - Bulgaria
  "br-FR" => ["047e:0000040c"], # Breton - France
  "ca-ES" => ["0403:0000040a"], # Catalan - Spain
  "cs-CZ" => ["0405:00000405"], # Czech - Czech Republic
  "cy-GB" => ["0452:00000452", "0809:00000809"], # Welsh - Great Britain
  "da-DK" => ["0406:00000406", "0409:00000406"], # Danish - Denmark
  "de-DE" => ["0407:00000407"], # German - Germany
  "dv-MV" => ["0465:00000465"], # Divehi - Maldives
  "el-GR" => ["0408:00000408"], # Greek - Greece
  "en-GB" => ["0809:00000809"], # English - Great Britain
  "en-US" => ["0409:00000409"], # English - United States
  "es-ES" => ["0c0a:0000040a"], # Spanish - Spain
  "et-EE" => ["0425:00000425"], # Estonian - Estonia
  "eu-ES" => ["042d:0000040a"], # Basque - Basque
  "fa-IR" => ["0429:00000429"], # Persian
  "fi-FI" => ["040b:0000040b"], # Finnish - Finland
  "fo-FO" => ["0438:00000406"], # Faroese - Faroe Islands
  "fr-FR" => ["040c:0000040c"], # French - France
  "ga-IE" => ["083c:00001809"], # Irish - Ireland
  "gd-GB" => ["0491:00011809"], # Scottish Gaelic - Great Britain
  "gl-ES" => ["0456:0000040a"], # Galician - Galician
  "gu-IN" => ["0447:00000447"], # Gujarati - India (Gujarati Script)

   # Note : Windows >= 8 probably uses 040d:0002040d instead
  "he-IL" => ["040d:0000040d"], # Hebrew - Israel

  "hi-IN" => ["0439:00010439"], # Hindi - India
  "hr-HR" => ["041a:0000041a"], # Croatian - Croatia
  "hu-HU" => ["040e:0000040e"], # Hungarian - Hungary
  "hy-AM" => ["042b:0002042b"], # Armenian - Armenia
  "id-ID" => ["0421:00000409"], # Indonesian - Indonesia
  "is-IS" => ["040f:0000040f"], # Icelandic - Iceland
  "it-CH" => ["0810:0000100c", "0810:00000410"], # Italian - Switzerland
  "it-IT" => ["0410:00000410"], # Italian - Italy
  "ja-JP" => ["0411:{03B5835F-F03C-411B-9CE2-AA23E1171E36}{A76C93D9-5523-4E90-AAFA-4DB112F9AC76}"], # Japanese - Japan
  "ka-GE" => ["0437:00010437"], # Georgian - Georgia
  "kk-KZ" => ["043f:0000043f"], # Kazakh - Kazakhstan
  "kn-IN" => ["044b:0000044b"], # Kannada - India (Kannada Script)
  "ko-KR" => ["0412:{A028AE76-01B1-46C2-99C4-ACD9858AE02F}{B5FE1F02-D5F2-4445-9C03-C568F23C99A1}"], # Korean(Extended Wansung) - Korea
  "ky-KG" => ["0440:00000440"], # Kyrgyz - Kyrgyzstan
  "lt-LT" => ["0427:00010427"], # Lithuanian - Lithuania

  # Note: Windows >= 8 probably uses 0426:00020426 instead
  "lv-LV" => ["0426:00000426"], # Latvian - Standard

  "mk-MK" => ["042f:0001042f"], # Macedonian
  "ml-IN" => ["044c:0000044c"], # Malayalam - India (Malayalam Script)
  "mn-MN" => ["0450:00000450"], # Mongolian (Cyrillic) - Mongolia
  "mr-IN" => ["044e:0000044e"], # Marathi - India
  "ms-BN" => ["083e:00000409"], # Malay - Brunei
  "ms-MY" => ["043e:00000409"], # Malay - Malaysia
  "nb-NO" => ["0414:00000414"], # Norwegian - Norway (Bokmal)
  "nl-BE" => ["0813:00000813"], # Dutch - Belgium
  "nl-NL" => ["0413:00020409"], # Dutch - Netherlands
  "or-IN" => ["0448:00000448"], # Odia - India (Odia Script)
  "pa-IN" => ["0446:00000446"], # Punjabi - India (Gurmukhi Script)
  "pl-PL" => ["0415:00000415"], # Polish - Poland
  "pt-BR" => ["0416:00000416"], # Portuguese - Brazil
  "pt-PT" => ["0816:00000816"], # Portuguese - Portugal
  "ro-RO" => ["0418:00010418"], # Romanian - Romania
  "ru-RU" => ["0419:00000419"], # Russian
  "sa-IN" => ["044f:00000439"], # Sanskrit - India
  "sk-SK" => ["041b:0000041b"], # Slovak - Slovakia
  "sl-SI" => ["0424:00000424"], # Slovenian - Slovenia
  "sq-AL" => ["041c:0000041c"], # Albanian - Albania
  "sv-FI" => ["081d:0000041d"], # Swedish - Finland
  "sv-SE" => ["041d:0000041d"], # Swedish - Sweden
  "sw-KE" => ["0441:00000409"], # Swahili - Kenya
  "ta-IN" => ["0449:00000449"], # Tamil - India
  "te-IN" => ["044a:0000044a"], # Telugu - India (Telugu Script)
  "th-TH" => ["041e:0000041e"], # Thai - Thailand
  "tr-TR" => ["041f:0000041f"], # Turkish - Turkey
  "uk-UA" => ["0422:00020422"], # Ukrainian - Ukraine
  "ur-PK" => ["0420:00000420"], # Urdu (Islamic Republic of Pakistan)
  "zh-CN" => ["0804:00000804"], # Chinese - PRC
  "zh-TW" => ["0404:00000404"], # Chinese - Taiwan
);

# Some Windows locale names are different and do not match the usual format
my %LocaleInfoKeys = (
  "mn-CN" => "mn-Mong",
  "mn-MN" => "mn-Mong-MN",
  "zh-CN" => "zh-Hans-CN",
);


#
# Command line processing
#

my $Usage;
sub CheckValue($$)
{
  my ($Option, $Value) = @_;

  if (defined $Value)
  {
    Error("$Option can only be specified once\n");
    $Usage = 2; # but continue processing this option
  }
  if (!@ARGV)
  {
    Error("missing value for $Option\n");
    $Usage = 2;
    return undef;
  }
  return shift @ARGV;
}

sub CheckLocale($$)
{
  my ($Option, $Value) = @_;

  $Value = CheckValue($Option, $Value);
  return undef if (!defined $Value);

  $Value =~ s/_/-/g;
  return $Value if ($Value =~ /^(?:[a-z]{2})-(?:Latn-)?(?:[A-Z]{2})$/);

  Error("'$Value' is not a valid $Option locale\n");
  $Usage = 2;
  return undef;
}

my ($OptVM, $OptHostName, $OptShow, $OptSysCopy, $OptDefCopy, $OptUseIntl, $OptReboot);
my ($OptDefault, $OptLocale, $OptCountry, $OptSystem, $OptUTF8, $OptMUI, $OptKeyboard);
while (@ARGV)
{
  my $Arg = shift @ARGV;
  if ($Arg eq "--help")
  {
    $Usage = 0;
  }
  elsif ($Arg eq "--vm")
  {
    $OptVM = CheckValue($Arg, $OptVM);
  }
  elsif ($Arg eq "--show")
  {
    $OptShow = 1;
  }
  elsif ($Arg eq "--use-intl")
  {
    $OptUseIntl = 1;
  }
  elsif ($Arg eq "--sys-copy")
  {
    $OptSysCopy = 1;
  }
  elsif ($Arg eq "--no-sys-copy")
  {
    $OptSysCopy = 0;
  }
  elsif ($Arg eq "--def-copy")
  {
    $OptDefCopy = 1;
  }
  elsif ($Arg eq "--no-def-copy")
  {
    $OptDefCopy = 0;
  }
  elsif ($Arg eq "--reboot")
  {
    if (defined $OptReboot and !$OptReboot)
    {
      Error("--reboot and --no-reboot are incompatible\n");
      $Usage = 2;
    }
    $OptReboot = 1;
  }
  elsif ($Arg eq "--no-reboot")
  {
    if ($OptReboot)
    {
      Error("--reboot and --no-reboot are incompatible\n");
      $Usage = 2;
    }
    $OptReboot = 0;
  }
  elsif ($Arg eq "--default")
  {
    $OptDefault = CheckLocale($Arg, $OptDefault);
  }
  elsif ($Arg eq "--locale")
  {
    $OptLocale = CheckLocale($Arg, $OptLocale);
  }
  elsif ($Arg eq "--country")
  {
    $OptCountry = CheckValue($Arg, $OptCountry);
  }
  elsif ($Arg eq "--system")
  {
    $OptSystem = CheckLocale($Arg, $OptSystem);
  }
  elsif ($Arg eq "--utf8")
  {
    $OptUTF8 = 1;
  }
  elsif ($Arg eq "--mui")
  {
    $OptMUI = CheckLocale($Arg, $OptMUI);
  }
  elsif ($Arg eq "--keyboard")
  {
    $OptKeyboard = CheckLocale($Arg, $OptKeyboard);
  }
  elsif ($Arg eq "--debug")
  {
    $Debug = 1;
  }
  elsif ($Arg =~ /^-/)
  {
    Error("unknown option '$Arg'\n");
    $Usage = 2;
  }
  elsif (!defined $OptHostName)
  {
    $OptHostName = $Arg;
  }
  else
  {
    Error("unexpected argument '$Arg'\n");
    $Usage = 2;
  }
}

# Check and untaint parameters
my ($VM, $Keyboard, $CountryId);
if (!defined $Usage)
{
  if (defined $OptVM)
  {
    my $VMs = CreateVMs();
    $VM = $VMs->GetItem($OptVM);
    if (!$VM)
    {
      Error("could not find the '$OptVM' VM\n");
      $Usage = 2;
    }
    elsif (!$Debug and $VM->Role eq "deleted")
    {
      Error("'$OptVM' is marked for deletion\n");
      $Usage = 2;
    }
  }
  elsif (!defined $OptHostName)
  {
    Error("you must specify the Windows machine to work on\n");
    $Usage = 2;
  }
  elsif ($OptHostName =~ /^([a-zA-Z0-9.-]+)$/)
  {
    $OptHostName = $1; # untaint
  }
  else
  {
    Error("'$OptHostName' is not a valid hostname\n");
    $Usage = 2;
  }

  $OptLocale ||= $OptDefault;
  $OptSystem ||= $OptDefault;
  $OptMUI ||= $OptDefault;
  $Keyboard = $OptKeyboard || $OptDefault;
  if (!$OptCountry and ($OptDefault || "") =~ /-([A-Z]{2})$/)
  {
    $OptCountry = $1;
  }

  # The "mixed" locale is used to make sure the tests each use the right
  # locale API. To do so set each locale setting to a different value, each
  # producing different results (e.g. different date format, decimal separator,
  # etc).
  my $MXStr = "";
  sub SetMX($$$)
  {
    my ($Value, $Option, $MXValue) = @_;
    return $Value if (!$Value or $Value !~ /^(?:mx-|MX$)/);
    $MXStr .= " $Option $MXValue";
    return $MXValue;
  }
  $OptLocale = SetMX($OptLocale, "--locale", "fr-FR");
  $OptCountry = SetMX($OptCountry, "--country", "TO");
  $OptSystem = SetMX($OptSystem, "--system", "ja-JP");
  $OptMUI = SetMX($OptMUI, "--mui", "ko-KR");
  $Keyboard = SetMX($Keyboard, "--keyboard", "es-ES");
  Info("using$MXStr\n") if ($MXStr);

  if ($OptCountry)
  {
    $CountryId = $Countries{$OptCountry};
    if (!defined $CountryId)
    {
      Error("unknown country '$OptCountry'\n");
      $Usage = 2;
    }
  }

  if (!$OptLocale and !$OptCountry and !$OptSystem and !$OptUTF8 and
      !$OptMUI and !$Keyboard and !defined $OptSysCopy and !defined $OptDefCopy)
  {
    if (!$OptShow)
    {
      Error("you must specify at least one locale to change\n");
      $Usage = 2;
    }
    if ($OptUseIntl or defined $OptReboot)
    {
      Error("--use-intl and --(no-)reboot can only be used when changing a locale\n");
      $Usage = 2;
    }
  }
  if ($OptShow and ($OptLocale or $OptCountry or $OptSystem or $OptUTF8 or
                    $OptMUI or $Keyboard or defined $OptSysCopy or
                    defined $OptDefCopy or defined $OptUseIntl or
                    defined $OptReboot))
  {
    Error("--show and the locale options are mutually incompatible\n");
    $Usage = 2;
  }

  $OptSysCopy = 1 if (!defined $OptSysCopy);
  $OptDefCopy = 1 if (!defined $OptDefCopy);

  # Two settings only take effect after a reboot:
  # - System locale changes.
  # - Display language changes only require a log out + log in but that cannot
  #   be automated so just reboot instead.
  $OptReboot = 1 if (!defined $OptReboot and ($OptSystem or $OptMUI));
}
if (defined $Usage)
{
  if ($Usage)
  {
    Error("try '$name0 --help' for more information\n");
    exit $Usage;
  }
  print "Usage: $name0 [options] --show (HOSTNAME|--vm VM)\n";
  print "or     $name0 [options] [--default DEF] [--locale LOC] [--country CTY] [--system SYS] [--utf8] [--mui MUI] [--keyboard KBD] [--no-sys-copy] [--no-def-copy] [--use-intl] [--no-reboot] (HOSTNAME|--vm VM)\n";
  print "\n";
  print "Sets the locale of the specified Windows machine.\n";
  print "\n";
  print "Where:\n";
  print "  HOSTNAME       Work on the specified Windows host (must be running TestAgentd).\n";
  print "  --show         Show the locale settings of the specified host and exit.\n";
  print "  --vm VM        Work on the specified TestBot VM, using its TestAgent\n";
  print "                 connection parameters.\n";
  print "  --default DEF  Use this Windows locale as the default for the other options.\n";
  print "                 The locale must be in a form suitable for Windows' intl.cpl\n";
  print "                 control panel module, that is roughly ll-CC where ll is an\n";
  print "                 ISO 639-1 language code and CC an ISO 3166-1 alpha-2 country\n";
  print "                 code.\n";
  print "  --locale LOC   Specifies the user formats (see --defaults).\n";
  print "                 . Windows 10 may reset this setting a few seconds after a\n";
  print "                   the next log in. Thus one should not expect this change to\n";
  print "                   persist after a reboot.\n";
  print "                 . Windows 10 GUI: Time & language -> Region -> Regional\n";
  print "                   format.\n";
  print "                 . APIs: GetUserDefaultLCID().\n";
  print "                 . Powershell: Set-Culture LOC\n";
  print "  --country CTY  Specifies the location using only the country part of the\n";
  print "                 Windows locale (see --defaults).\n";
  print "                 . Windows 10 may reset this setting a few seconds after a\n";
  print "                   the next log in. Thus one should not expect this change to\n";
  print "                   persist after a reboot.\n";
  print "                 . Windows 10 GUI: Time & language -> Region -> Country.\n";
  print "                 . APIs: GetUserGeoID().\n";
  print "                 . Powershell: Set-WinHomeLocation -GeoId CTY-ID\n";
  print "  --system SYS   Specifies the system locale (see --defaults).\n";
  print "                 . This requires elevated privileges.\n";
  print "                 . Only takes effect after a reboot.\n";
  print "                 . Windows only allows using a Unicode-only locale (such as\n";
  print "                   hi-IN) as the system locale when setting the code pages to\n";
  print "                   UTF-8.\n";
  print "                 . Windows 10 GUI: Time & language -> Language ->\n";
  print "                   Administrative Language Settings -> Change system locale.\n";
  print "                 . APIs: GetSystemDefaultLCID(), GetThreadLocale().\n";
  print "                 . Powershell: Set-WinSystemLocale -SystemLocale SYS\n";
  print "  --utf8         Set the code pages (ACP, MACCP and OEMCP) to UTF-8.\n";
  print "                 . This requires elevated privileges.\n";
  print "                 . Only supported on Windows 10+. This will render older Windows\n";
  print "                   versions unbootable.\n";
  print "                 . Windows 10 GUI: Time & language -> Language ->\n";
  print "                   Administrative Language Settings -> Change system locale ->\n";
  print "                   Beta: Use Unicode UTF-8 for worldwide language support.\n";
  print "                 . APIs: GetACP(), GetOEMCP().\n";
  print "                 . Powershell: Automatically and unconditionally set by\n";
  print "                               Set-WinSystemLocale for the relevant locales.\n";
  print "  --mui MUI      Specifies the display language (see --defaults).\n";
  print "                 . Only takes effect after a log out + log in.\n";
  print "                 . Windows sometimes uses a different locale string. For\n";
  print "                   instance setting the display language to English (Canada) sets\n";
  print "                   this to en-GB instead of en-CA.\n";
  print "                 . Windows 10 GUI: Time & language -> Language -> Windows\n";
  print "                   display language.\n";
  print "                 . APIs: GetSystemPreferredUILanguages() (--sys-copy case),\n";
  print "                         GetUserDefaultUILanguage(), GetThreadUILanguage().\n";
  print "                 . Powershell: Set-WinUILanguageOverride -Language MUI\n";
  print "  --keyboard KBD Specifies the keyboard layout (see --defaults).\n";
  print "                 . Windows 10 GUI: Time & language -> Language -> Keyboard ->\n";
  print "                   Override for default input method.\n";
  print "                 . Powershell: Set-WinDefaultInputMethodOverride -InputTip KBD-ID\n";
  print "  --sys-copy     Copy the current locales (--locale --country --mui --keyboard)\n";
  print "                 to the system accounts (System/LocalSystem, NT Authority) and\n";
  print "                 in particular the one used by the logon screen. This is the\n";
  print "                 default.\n";
  print "                 . This requires elevated privileges.\n";
  print "                 . Windows 10 GUI: Time & language -> Language -> Administrative\n";
  print "                   language settings -> Copy Settings -> Welcome Screen and\n";
  print "                   system accounts.\n";
  print "                 . Powershell (Windows 11+): Copy-UserInternationalSettingsToSystem -WelcomeScreen \$True\n";
  print "                 . Intl.cpl: CopySettingsToSystemAcct='true'\n";
  print "  --no-sys-copy  Do not copy the current locale to the system accounts such as\n";
  print "                 the one used by the logon screen.\n";
  print "  --def-copy     Copy the current locales (--locale --country --mui --keyboard)\n";
  print "                 to the default user account. This is the default.\n";
  print "                 . Windows 10 GUI: Time & language -> Language -> Administrative\n";
  print "                   language settings -> Copy Settings -> New user accounts.\n";
  print "                 . Powershell (Windows 11+): Copy-UserInternationalSettingsToSystem -NewUser \$True\n";
  print "                 . Intl.cpl: CopySettingsToDefaultUserAcct='true'\n";
  print "  --no-def-copy  Do not copy the current locales to the default user account.\n";
  print "  --use-intl     Change the locale using the intl.cpl control panel module\n";
  print "                 instead of Powershell. This option should only be used for\n";
  print "                 debugging.\n";
  print "  --no-reboot    Do not reboot Windows. Some locale changes will only take\n";
  print "                 effect after the next reboot. This will also prevent working\n";
  print "                 around cases where some locale changes do not stick after a\n";
  print "                 reboot. So this should only be used for debugging.\n";
  print "  --debug        Show more detailed information about progress.\n";
  print "  --help         Shows this usage message.\n";
  exit 0;
}

$TA = $VM ? $VM->GetAgent() : TestAgent->new($OptHostName, $AgentPort);


#
# Registry helpers
#

sub GetRunError($)
{
  my ($ExitCode) = @_;
  return  $TA->GetLastError() || "exit code $ExitCode";
}

sub RegGetValues($;$)
{
  my ($Key, $VName) = @_;

  my $Cmd = ["reg.exe", "query", $Key];
  if ($VName)
  {
    push @$Cmd, ($VName =~ /\*/ ? "/f" : "/v"), $VName;
  }
  else
  {
    push @$Cmd, "/ve";
  }

  my $Ret = $TA->RunAndWait($Cmd, 0, 10, undef, "reg.out");
  FatalError("failed to run @$Cmd: ", $TA->GetLastError(), "\n") if ($Ret < 0);
  my $RegOut = $TA->GetFileToString("reg.out") if (!$Ret);
  $TA->Rm("reg.out");
  return {} if ($Ret); # Presumably the registry key does not exist

  my $Values = {};
  foreach my $Line (split /\n/, $RegOut)
  {
    if ($Line =~ /^\s{4}(.*)\s{4}REG_[A-Z_]+\s{4}(.*)\r$/)
    {
      my ($VName, $Value) = ($1, $2);
      $Value = [split /\\0/, $Value] if ($Value =~ /\\0/);
      $Values->{$VName} = $Value;
    }
  }
  return $Values;
}

sub RegGetValue($;$)
{
  my ($Key, $VName) = @_;
  my $Values = RegGetValues($Key, $VName);
  return $Values->{defined $VName ? $VName : "(Default)"};
}


#
# Show the host's locale settings
#

if (!$TA->SendFile("$0.ps1", "$name0.ps1", 0))
{
  FatalError("could not send the Powershell script:", $TA->GetLastError(), "\n");
}

sub GetWinProperties($)
{
  my ($Cmd) = @_;

  Debug(Elapsed($Start), " Running: @$Cmd\n");
  my $Ret = $TA->RunAndWait($Cmd, 0, 30, undef, "$name0.out", "$name0.out");
  FatalError("failed to run @$Cmd: ", $TA->GetLastError(), "\n") if ($Ret < 0);
  my $Out = $TA->GetFileToString("$name0.out") if (!$Ret);

  my ($Settings, $Key);
  foreach my $Line (split /\n/, $Out || "")
  {
    $Line =~ s/\r$//;
    if ($Line =~ /^([a-zA-Z]+)=\[(.*)\]$/)
    {
      my ($Name, $Value) = ($1, $2);
      $Settings->{$Name} = [split /, /, $Value];
    }
    elsif ($Line =~ /^([a-zA-Z]+)=(.*)$/)
    {
      my ($Name, $Value) = ($1, $2);
      $Settings->{$Name} = $Value eq "<undef>" ? undef : $Value;
    }
  }
  return $Settings;
}

sub GetWinSettings()
{
  return GetWinProperties(["powershell.exe", "-ExecutionPolicy", "ByPass",
                           "-File", "$name0.ps1", "settings"]);
}

sub Value2Str($)
{
  my ($Value) = @_;
  return !defined $Value ? "<undef>" :
         ref($Value) ne "ARRAY" ? $Value :
         ("[". join(", ", @$Value) ."]");
}

sub ShowWinSettings($)
{
  my ($Settings) = @_;

  print "Current account:\n";
  print "Locale            (--locale) = ", Value2Str($Settings->{Locale}), "\n";
  print "LocaleName        (--locale) = ", Value2Str($Settings->{LocaleName}), "\n";
  print "Geo:Nation       (--country) = ", Value2Str($Settings->{Country}), "\n";
  print "Geo:Name         (--country) = ", Value2Str($Settings->{CountryName}), "\n";
  print "Languages                    = ", Value2Str($Settings->{Languages}), "\n";
  print "PreferredUILanguages (--mui) = ", Value2Str($Settings->{PreferredUILanguages}), "\n";
  print "          ...Pending (--mui) = ", Value2Str($Settings->{PreferredUILanguagesPending}), "\n";
  print "         Previous...         = ", Value2Str($Settings->{PreviousPreferredUILanguages}), "\n";
  print "WindowsOverride      (--mui) = ", Value2Str($Settings->{WindowsOverride}), "\n";
  print "InputMethod     (--keyboard) = ", Value2Str($Settings->{InputMethod}), "\n";

  print "\n";
  print "System settings:\n";
  print "Nls:Language      (--system) = ", Value2Str($Settings->{SysLanguage}), "\n";
  print "Nls:Locale        (--system) = ", Value2Str($Settings->{SysLocale}), "\n";
  print "Nls:InstallLang              = ", Value2Str($Settings->{SysInstallLang}), "\n";
  print "ACP                 (--utf8) = ", Value2Str($Settings->{ACP}), "\n";
  print "MACCP               (--utf8) = ", Value2Str($Settings->{MACCP}), "\n";
  print "OEMCP               (--utf8) = ", Value2Str($Settings->{OEMCP}), "\n";

  print "\n";
  print ".DEFAULT account (see --sys-copy):\n";
  # Locale used for the date and time in the logon screen
  print "Locale            (--locale) = ", Value2Str($Settings->{DefLocale}), "\n";
  print "LocaleName        (--locale) = ", Value2Str($Settings->{DefLocaleName}), "\n";
  print "Geo:Nation       (--country) = ", Value2Str($Settings->{DefCountry}), "\n";
  print "Geo:Name         (--country) = ", Value2Str($Settings->{DefCountryName}), "\n";
  # Language of the 'Welcome' message
  print "PreferredUILanguages         = ", Value2Str($Settings->{DefPreferredUILanguages}), "\n";
  print "                 ...Pending  = ", Value2Str($Settings->{DefPreferredUILanguagesPending}), "\n";
  print "                 Previous... = ", Value2Str($Settings->{DefPreviousPreferredUILanguages}), "\n";
  # Language of the 'Password' text in the password field
  print "MachinePrefUILanguages(--mui)= ", Value2Str($Settings->{DefMachinePreferredUILanguages}), "\n";
  # Keyboard layout for the password field
  print "InputMethod     (--keyboard) = ", Value2Str($Settings->{DefInputMethod}), "\n";
}

my $InitialSettings = GetWinSettings();
if ($OptShow)
{
  ShowWinSettings($InitialSettings);
  Cleanup();
  exit(0);
}


#
# Helper to get the LCIDs
#

my (%WinLCIDs, %WinKeyboardIds);

sub GetWinKeyboardIds($)
{
  my ($Locale) = @_;

  my $Info = GetWinProperties(["powershell.exe", "-ExecutionPolicy", "ByPass",
                               "-File", "$name0.ps1", "info", $Locale]);
  $WinKeyboardIds{$Locale} = $Info->{InputMethodTips} ? [$Info->{InputMethodTips}] : $Keyboards{$Locale};
  Debug("WinKeyboardIds=", Value2Str($WinKeyboardIds{$Locale}), "\n");
  if (!$WinKeyboardIds{$Locale})
  {
    FatalError("could not find the $Locale LCID and keyboard ids. Maybe the locale is not installed?\n");
  }

  # The first component of the keyboard id is the locale's LCID.
  my $WinLCID = uc($WinKeyboardIds{$Locale}->[0]);
  $WinLCID =~ s/:.*$//;
  $WinLCIDs{$Locale} = $WinLCID;

  # Check our builtin information
  if ($Keyboards{$Locale})
  {
    my $LCID = uc($Keyboards{$Locale}->[0]);
    $LCID =~ s/:.*$//;
    if ($LCID ne $WinLCID)
    {
      FatalError("inconsistent $Locale LCID: expected $LCID, got $WinLCID\n");
    }
  }

  return $WinKeyboardIds{$Locale};
}

sub GetLCID($)
{
  my ($Locale) = @_;
  return undef if (!$Locale);

  GetWinKeyboardIds($Locale) if (!$WinLCIDs{$Locale});
  return $WinLCIDs{$Locale};
}

sub GetKeyboardIds($)
{
  my ($Locale) = @_;
  return undef if (!$Locale);

  GetWinKeyboardIds($Locale) if (!$WinKeyboardIds{$Locale});
  return $WinKeyboardIds{$Locale};
}

my $LCIDLocale = GetLCID($OptLocale);
my $LCIDSystem = GetLCID($OptSystem);

my $KeyboardIds = GetKeyboardIds($Keyboard);
if (!$KeyboardIds and $Keyboard)
{
  if ($OptKeyboard)
  {
    FatalError("no known keyboard for the $OptKeyboard locale\n");
  }
  # intl.cpl automatically pick the appropriate keyboard but, unlike
  # for Windows' initial installation, it does not make it the
  # default since the system has a keyboard already.
  Warning("no known keyboard for the $Keyboard locale. Letting intl.cpl use its default.\n");
}


#
# Helper to change the Windows locales
#

sub SetWinLocales($$$$$$$$)
{
  my ($SysCopy, $DefCopy, $Locale, $CountryId, $System, $UTF8, $MUI, $KeyboardIds) = @_;

  my $Cmd = ["powershell.exe", "-ExecutionPolicy", "ByPass", "-File",
             "$name0.ps1", "locales", $Locale || ".", $CountryId || ".",
             $System || ".", $UTF8 ? "force" : "no", $MUI || ".",
             $KeyboardIds ? $KeyboardIds->[0] : ".",
             $SysCopy ? "true" : "false", $DefCopy ? "true" : "false",
             $OptUseIntl ? "true" : "false"];
  Debug(Elapsed($Start), " Running: @$Cmd\n");
  my $Ret = $TA->RunAndWait($Cmd, 0, 30, undef, "$name0.out", "$name0.out");
  FatalError("$name0.ps1 locales failed: ", $TA->GetLastError(), "\n") if ($Ret < 0);
  my $Out = $TA->GetFileToString("$name0.out");
  foreach my $Line (split /\n/, $Out || "")
  {
    $Line =~ s/\r$//;
    Info("$Line\n");
    if ($Line eq "Setting the system locale to en-US instead")
    {
      # For the final checks
      $OptSystem = "en-US";
      $LCIDSystem = "0409";
    }
    elsif ($Line =~ /^Windows uses (.*) for the MUI language/)
    {
      $OptMUI = $1; # for the final check
    }
  }
}

SetWinLocales($OptSysCopy, $OptDefCopy, $OptLocale, $CountryId,
              $OptSystem, $OptUTF8, $OptMUI, $KeyboardIds);


#
# Reboot to finalize the changes
#

if ($OptReboot)
{
  my $Cmd = ["shutdown.exe", "/r", "/d", "00:00", "/t", "0"];
  Debug(Elapsed($Start), " Rebooting: @$Cmd\n");
  my $OldCount =  $TA->GetProperties("start.count");
  $TA->Run($Cmd, 0);
  # Note that we cannot wait for this command since it reboots Windows

  my ($OneTimeout, $MinAttempts, $MinTimeout) = $TA->SetConnectTimeout(3, 1, 3);
  while (1)
  {
    my $NewCount = $TA->GetProperties("start.count");
    next if (!defined $NewCount); # Rebooting (TestAgentd is not running)
    last if ($NewCount != $OldCount); # start.count got incremented
    sleep(10); # Not rebooting yet
  }
  $TA->SetConnectTimeout($OneTimeout, $MinAttempts, $MinTimeout);

  # Skip this check on pre-Windows 10 versions, i.e. those that don't have
  # 'CountryName' and if the settings were not changed in the first place.
  if ($InitialSettings->{CountryName} and
      (($OptLocale and $InitialSettings->{Locale} ne $OptLocale) or
       ($OptCountry and $InitialSettings->{Country} ne $CountryId)))
  {
    # On Windows 10 21H1 some process typically resets the --locale and
    # --country some time after the login!!!
    # This also happens when making the change through the Powershell's
    # Set-Culture and Set-WinHomeLocation calls, but not when manually going
    # through the GUI.
    # So wait for the reset to happen and re-set them :-(
    my $Expected = $OptLocale ? "--locale $OptLocale" : "";
    $Expected .= !$OptCountry ? "" : (($Expected ? " and " : "") . ("--country $OptCountry"));
    Debug(Elapsed($Start), " Expecting $Expected to get reverted\n");
    foreach (1..30)
    {
      my $Reverted;
      if ($OptCountry)
      {
        my $Nation = RegGetValue($HKCU_GEO, "Nation");
        $Reverted = "--country $Nation" if ($Nation ne $CountryId);
      }
      else
      {
        my $LocaleName = RegGetValue($HKCU_INTERNATIONAL, "LocaleName");
        $Reverted = "--locale $LocaleName" if ($LocaleName ne $OptLocale);
      }
      if ($Reverted)
      {
        Warning("$Expected got reverted to $Reverted. Setting them again but this will not stick across reboots\n");
        SetWinLocales($OptSysCopy, $OptDefCopy, $OptLocale, $CountryId, undef, undef, undef, undef);
        last;
      }
      sleep(2);
    }
  }
}


#
# Check that all the changes succeeded
#

my $Success = 1;
sub CheckSetting($$$$;$)
{
  my ($Settings, $VName, $Expected, $For, $IgnoreCase) = @_;

  my $Value = $Settings->{$VName};
  if (defined $Value and $Value !~ /^<.+>$/)
  {
    if ((!$IgnoreCase and $Value ne $Expected) or
        ($IgnoreCase and uc($Value) ne uc($Expected)))
    {
      Error("expected $VName = $Expected but got $Value $For\n");
      $Success = 0;
    }
  }
}

Debug(Elapsed($Start), " Checking the new locale settings\n");
my $Settings = GetWinSettings();
ShowWinSettings($Settings) if ($Debug);
if ($OptLocale)
{
  CheckSetting($Settings, "Locale", "0000$LCIDLocale", "for --locale $OptLocale", 1);
  CheckSetting($Settings, "LocaleName", $OptLocale, "for --locale $OptLocale");
}
if ($OptCountry)
{
  CheckSetting($Settings, "Country", $CountryId, "for --country $OptCountry");
  CheckSetting($Settings, "CountryName", $OptCountry, "for --country $OptCountry");
}
if ($OptSystem)
{
  CheckSetting($Settings, "SysLanguage", $LCIDSystem, "for --system $OptSystem", 1);
  CheckSetting($Settings, "SysLocale", "0000$LCIDSystem", "for --system $OptSystem", 1);
}
if ($OptUTF8)
{
  foreach my $VName ("ACP", "MACCP", "OEMCP")
  {
    CheckSetting($Settings, $VName, "65001", "for --utf8");
  }
}
if ($OptMUI)
{
  if ($OptReboot)
  {
    CheckSetting($Settings, "PreferredUILanguages", $OptMUI, "for --mui $OptMUI");
    if ($Settings->{PreferredUILanguagesPending})
    {
      Error("PreferredUILanguagesPending should not be set for --mui $OptMUI\n");
      $Success = 0;
    }
  }
  else
  {
    CheckSetting($Settings, "PreferredUILanguagesPending", $OptMUI, "for --mui $OptMUI");
  }
}
if ($KeyboardIds)
{
  CheckSetting($Settings, "InputMethod", $KeyboardIds->[0], "for --keyboard $Keyboard", 1);
}

if ($OptSysCopy)
{
  if ($OptLocale)
  {
    CheckSetting($Settings, "DefLocale", "0000$LCIDLocale", "for --locale $OptLocale", 1);
    CheckSetting($Settings, "DefLocaleName", $OptLocale, "for --locale $OptLocale");
  }
  if ($OptCountry)
  {
    CheckSetting($Settings, "DefCountry", $CountryId, "for --country $OptCountry");
    CheckSetting($Settings, "DefCountryName", $OptCountry, "for --country $OptCountry");
  }
  if ($OptMUI and $OptReboot)
  {
    CheckSetting($Settings, "DefMachinePreferredUILanguages", $OptMUI, "for --mui $OptMUI");
  }
  if ($KeyboardIds)
  {
    CheckSetting($Settings, "DefInputMethod", $KeyboardIds->[0], "for --keyboard $Keyboard", 1);
  }
}
if (!$OptReboot and ($OptSystem or $OptMUI))
{
  my @Pending;
  push @Pending, "--system $OptSystem" if ($OptSystem);
  push @Pending, "--mui $OptMUI" if ($OptMUI);
  Warning(join(" ", "the", @Pending, "changes are still pending\n"));
}

Cleanup();
exit(1) if (!$Success);
Debug(Elapsed($Start), " All done!\n");
exit(0);
