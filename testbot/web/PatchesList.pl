# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Patch list page
#
# Copyright 2010 Ge van Geldorp
# Copyright 2012-2018, 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;

package PatchesBlock;

use ObjectModel::CGI::CollectionBlock;
our @ISA = qw(ObjectModel::CGI::CollectionBlock);

use WineTestBot::Patches;


sub Create($$)
{
  my ($Collection, $EnclosingPage) = @_;

  return PatchesBlock->new($Collection, $EnclosingPage);
}

sub DisplayProperty($$)
{
  my ($self, $PropertyDescriptor) = @_;

  my $PropertyName = $PropertyDescriptor->GetName();
  return $PropertyName eq "Received" ? ("ro", "daytimetipdate") :
         $PropertyName =~ /^(?:Disposition|FromName|Subject)$/ ? "ro" :
         "";
}

sub GenerateDataView($$$)
{
  my ($self, $Row, $Col) = @_;

  if ($Col->{Descriptor}->GetName() eq "Disposition" and
      $Row->{Item}->Disposition =~ /job ([0-9]+)$/)
  {
    my $JobId = $1;
    my $Title = $Row->{Item}->WebPatchId;
    $Title = " title='Web patch $Title'" if ($Title);
    print "<a href='/JobDetails.pl?Key=$JobId'$Title>Job $JobId</a>";
  }
  else
  {
    $self->SUPER::GenerateDataView($Row, $Col);
  }
}


package main;

use ObjectModel::CGI::CollectionPage;
use WineTestBot::Patches;

my $Request = shift;
my $Page = ObjectModel::CGI::CollectionPage->new($Request, "", CreatePatches(), \&PatchesBlock::Create);
$Page->SetReadWrite(0);
$Page->SetRefreshInterval(60);
$Page->GeneratePage();
