#!/usr/bin/perl
#
# Gitweb replacement script to redirect URLs to Gitlab.
#
# Copyright 2024 Alexandre Julliard
#
# Parameter validation copied from the original gitweb.cgi:
#
# (C) 2005-2006, Kay Sievers <kay.sievers@vrfy.org>
# (C) 2005, Christian Gierke
#
# This program is licensed under the GPLv2

use strict;
use warnings;
use CGI qw(:standard -nosticky);
use CGI::Carp qw(fatalsToBrowser);
use Encode;

binmode STDOUT, ':utf8';

our $path_info = decode_utf8($ENV{"PATH_INFO"});
our $gitlab_url = "https://gitlab.winehq.org";

our %project_map =
(
    "appdb.git"     => "/winehq/appdb",
    "bugzilla.git"  => "/winehq/bugzilla",
    "fontforge.git" => "/wine/fontforge",
    "tools.git"     => "/winehq/tools",
    "vkd3d.git"     => "/wine/vkd3d",
    "website.git"   => "/winehq/winehq",
    "wine.git"      => "/wine/wine",
);

our %input_params = ();

our %cgi_param_mapping =
(
    project => "p",
    action => "a",
    file_name => "f",
    file_parent => "fp",
    hash => "h",
    hash_parent => "hp",
    hash_base => "hb",
    hash_parent_base => "hpb",
    order => "o",
    searchtext => "s",
);

my @actions =
(
    "blame",
    "blame_incremental",
    "blame_data",
    "blobdiff",
    "blobdiff_plain",
    "blob",
    "blob_plain",
    "commitdiff",
    "commitdiff_plain",
    "commit",
    "forks",
    "heads",
    "history",
    "log",
    "patch",
    "patches",
    "remotes",
    "rss",
    "atom",
    "search",
    "search_help",
    "shortlog",
    "summary",
    "tag",
    "tags",
    "tree",
    "snapshot",
    "object",
    "opml",
    "project_list",
    "project_index",
);

sub evaluate_path_info
{
	return if defined $input_params{'project'};
	return if !$path_info;
	$path_info =~ s,^/+,,;
	return if !$path_info;

	# find which part of PATH_INFO is project
	my $project = $path_info;
	$project =~ s,/+$,,;
	while ($project && !defined($project_map{$project})) {
		$project =~ s,/*[^/]*$,,;
	}
	return unless $project;
	$input_params{'project'} = $project;

	# do not change any parameters if an action is given using the query string
	return if $input_params{'action'};
	$path_info =~ s,^\Q$project\E/*,,;

	# next, check if we have an action
	my $action = $path_info;
	$action =~ s,/.*$,,;
        if (grep { $_ eq $action } @actions) {
            $path_info =~ s,^$action/*,,;
		$input_params{'action'} = $action;
	}

	# list of actions that want hash_base instead of hash, but can have no
	# pathname (f) parameter
	my @wants_base = (
		'tree',
		'history',
	);

	# we want to catch, among others
	# [$hash_parent_base[:$file_parent]..]$hash_parent[:$file_name]
	my ($parentrefname, $parentpathname, $refname, $pathname) =
		($path_info =~ /^(?:(.+?)(?::(.+))?\.\.)?([^:]+?)?(?::(.+))?$/);

	# first, analyze the 'current' part
	if (defined $pathname) {
		# we got "branch:filename" or "branch:dir/"
		# we could use git_get_type(branch:pathname), but:
		# - it needs $git_dir
		# - it does a git() call
		# - the convention of terminating directories with a slash
		#   makes it superfluous
		# - embedding the action in the PATH_INFO would make it even
		#   more superfluous
		$pathname =~ s,^/+,,;
		if (!$pathname || substr($pathname, -1) eq "/") {
			$input_params{'action'} ||= "tree";
			$pathname =~ s,/$,,;
		} else {
			# the default action depends on whether we had parent info
			# or not
			if ($parentrefname) {
				$input_params{'action'} ||= "blobdiff_plain";
			} else {
				$input_params{'action'} ||= "blob_plain";
			}
		}
		$input_params{'hash_base'} ||= $refname;
		$input_params{'file_name'} ||= $pathname;
	} elsif (defined $refname) {
		# we got "branch". In this case we have to choose if we have to
		# set hash or hash_base.
		#
		# Most of the actions without a pathname only want hash to be
		# set, except for the ones specified in @wants_base that want
		# hash_base instead. It should also be noted that hand-crafted
		# links having 'history' as an action and no pathname or hash
		# set will fail, but that happens regardless of PATH_INFO.
		if (defined $parentrefname) {
			# if there is parent let the default be 'shortlog' action
			# (for http://git.example.com/repo.git/A..B links); if there
			# is no parent, dispatch will detect type of object and set
			# action appropriately if required (if action is not set)
			$input_params{'action'} ||= "shortlog";
		}
		if ($input_params{'action'} &&
		    grep { $_ eq $input_params{'action'} } @wants_base) {
			$input_params{'hash_base'} ||= $refname;
		} else {
			$input_params{'hash'} ||= $refname;
		}
	}

	# next, handle the 'parent' part, if present
	if (defined $parentrefname) {
		# a missing pathspec defaults to the 'current' filename, allowing e.g.
		# someproject/blobdiff/oldrev..newrev:/filename
		if ($parentpathname) {
			$parentpathname =~ s,^/+,,;
			$parentpathname =~ s,/$,,;
			$input_params{'file_parent'} ||= $parentpathname;
		} else {
			$input_params{'file_parent'} ||= $input_params{'file_name'};
		}
		# we assume that hash_parent_base is wanted if a path was specified,
		# or if the action wants hash_base instead of hash
		if (defined $input_params{'file_parent'} ||
			grep { $_ eq $input_params{'action'} } @wants_base) {
			$input_params{'hash_parent_base'} ||= $parentrefname;
		} else {
			$input_params{'hash_parent'} ||= $parentrefname;
		}
	}
}

# quote unsafe chars, but keep the slash, even when it's not
# correct, but quoted slashes look too horrible in bookmarks
sub esc_param
{
	my $str = shift;
	return undef unless defined $str;
	$str =~ s/([^A-Za-z0-9\-_.~()\/:@ ]+)/CGI::escape($1)/eg;
	$str =~ s/ /\+/g;
	return $str;
}

our $cgi = CGI->new();

while (my ($name, $symbol) = each %cgi_param_mapping)
{
    $input_params{$name} = decode_utf8($cgi->param($symbol));
}

evaluate_path_info();

our $project = $input_params{project} || "";
our $dir = $project_map{$project} || "";
our $hash_base = esc_param($input_params{hash_base}) || "master";
our $hash = esc_param($input_params{hash}) || $hash_base;
our $file_name = esc_param($input_params{file_name}) || "";
our $searchtext = esc_param($input_params{searchtext}) || "";
our $action = $input_params{action};

if (!defined $action)
{
    if ($hash =~ /^info\// && $cgi->param("service") eq "git-upload-pack")
    {
        $action = 'git_upload_pack';
    }
    elsif (defined $input_params{hash} || defined $input_params{hash_base})
    {
        $action = 'blob';
    }
    elsif ($dir)
    {
        $action = 'summary';
    }
    else
    {
        $action = 'project_list';
    }
}

my %redirects =
(
    blame             => "$dir/-/blame/$hash_base/$file_name",
    blame_incremental => "$dir/-/blame/$hash_base/$file_name",
    blame_data        => "$dir/-/blame/$hash_base/$file_name",
    blob              => "$dir/-/blob/$hash_base/$file_name",
    blob_plain        => "$dir/-/raw/$hash_base/$file_name",
    blobdiff          => "$dir/-/commit/$hash",
    blobdiff_plain    => "$dir/-/commit/$hash.diff",
    commitdiff        => "$dir/-/commit/$hash",
    commitdiff_plain  => "$dir/-/commit/$hash.diff",
    commit            => "$dir/-/commit/$hash",
    forks             => "$dir/-/forks",
    heads             => "$dir/-/branches",
    history           => "$dir/-/commits/$hash_base/$file_name",
    log               => "$dir/-/commits/$hash",
    patch             => "$dir/-/commit/$hash.diff",
    patches           => "$dir/-/commit/$hash.diff",
    rss               => "$dir/-/commits/$hash?format=atom",
    atom              => "$dir/-/commits/$hash?format=atom",
    search            => "$dir/-/commits/$hash?search=$searchtext",
    shortlog          => "$dir/-/commits/$hash",
    summary           => "$dir",
    tag               => "$dir/-/tags/$hash",
    tags              => "$dir/-/tags",
    tree              => "$dir/-/tree/$hash_base/$file_name",
    object            => "$dir/-/blob/$hash_base/$file_name",
    opml              => "/explore/groups",
    project_list      => "/explore/groups",
    project_index     => "/explore/groups",
    git_upload_pack   => "$dir/$hash?service=git-upload-pack",
);

my $uri = $gitlab_url . ($redirects{$action} || $dir || $redirects{project_list});
print $cgi->redirect( -uri => $uri, -status => '301 Moved Permanently' );
exit 0;
