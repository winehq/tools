These tools are used to launch a libvirt gitlab-runner
which can start and stop libvirt machines to run Wine tests.

Prerequisites:
--------------
A system with a working libvirt configuration.  You should be
able to run virsh without trouble.

A GitLab instance.

Installation:
-------------
Install gitlab-runner onto the system and register it as a runner with
your GitLab instance.  If you stick with the default user of gitlab-runner,
make sure the gitlab-runner user can run virsh commands.  On Debian,
that means adding the gitlab-runner user to the libvirt group.

Using the config.toml file as a model, configure /etc/gitlab-runner/config.toml.
The keys are
  shell = "powershell"
  [runners.custom]
    prepare_exec = /path/to/this/directory/prepare.sh
    run_exec = /path/to/this/directory/run.sh
    cleanup_exec = /path/to/this/directory/cleanup.sh

Edit the config file to establish how to talk to libvirt, which VM (aka domain),
and which snapshot to use.


Provisioning a Windows VM:
--------------------------
In general, the idea is to create a standard Windows install, add the
qemu guest agent, and you're done.

Some notions about provision VMs are here:
  https://wiki.winehq.org/Wine_TestBot_VMs

The guest agent can be downloaded here:
  https://github.com/virtio-win/virtio-win-pkg-scripts/blob/master/README.md
Side note: there is a long chain of trust to deduce that; if you start at:
  https://wiki.qemu.org/Features/GuestAgent
you chain through deprecated and moved projects until you arrive at virtio-win.
Google rank also suggests that it is a good choice.

Note that you need to add a qemu channel to any qemu virtual machine to use
the agent.

Finally, in order to run applications on the desktop, I found that I needed to
run the agent manually from an administrator console, and then snapshot the VM
running in that state.  There are settings to allow a service to access the
desktop, but I could not get them to work.

Misc Notes:
----------
The gitlab runner will produce a warning about the deprecation of build_script.
There seems to be no way to disable that warning.  The code is here:
  https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/executors/custom/custom.go#L303
