import requests
import configparser
import sys


def fetch_all(url, settings):
    result = []
    while url:
        r = requests.get(url, headers={"PRIVATE-TOKEN": settings.GITLAB_TOKEN})
        r.raise_for_status()
        result += r.json()

        if 'next' in r.links:
            url = r.links['next']['url']
        else:
            url = None
    return result


class Settings:
    def __init__(self, fname):
        self.cp = configparser.ConfigParser()
        try:
            if len(self.cp.read(fname)) == 0:
                print(f"Error: invalid or missing configuration in {fname}.", file=sys.stderr)
                sys.exit(1)
        except configparser.MissingSectionHeaderError as e:
            print(f"Error: missing section header parsing {fname}")
            print(e)
            sys.exit(1)

        for s in ['DATABASE', 'GITLAB_TOKEN', 'GITLAB_URL', 'GITLAB_PROJECT_NAME',
                  'BRIDGE_FROM_EMAIL', 'BRIDGE_TO_EMAIL', 'BRIDGE_TAG', 'MAINTAINERS',
                  'SMTP_DOMAIN', 'SMTP_USER', 'SMTP_PASSWORD', 'RUNNER_COMMAND']:
            if s in self.cp['settings']:
                self.__dict__[s] = self.cp['settings'][s].strip('"')
            else:
                self.__dict__[s] = None

        for s in ['DEBUG', 'READ_ONLY']:
            if s in self.cp['settings']:
                self.__dict__[s] = self.cp['settings'].getboolean(s)
            else:
                self.__dict__[s] = None

        for s in ['GITLAB_PROJECT_ID', 'SMTP_PORT', 'INITIAL_BACKLOG_DAYS',
                  'MAXIMUM_PATCHES', 'RUNNER_INTERVAL']:
            if s in self.cp['settings']:
                self.__dict__[s] = self.cp['settings'].getint(s)
            else:
                self.__dict__[s] = None
